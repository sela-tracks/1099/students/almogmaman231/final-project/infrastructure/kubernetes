1. Download AWS CLI
2. configure AWS CLI for the role (not the root, more secured)
3. download terraform
4. adding the terraform.exe path to the PATH environment variable.
5. write a terraform main, variable, and network files for setting up an EKS service in AWS
6. Run terraform init, plan and apply - creating the EKS in AWS
7. connecting to the EKS: aws eks --region us-west-2 update-kubeconfig --name my-eks-cluster
8. adding images of the lens and AWS console.