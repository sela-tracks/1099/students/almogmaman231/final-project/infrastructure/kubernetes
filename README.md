# AWS EKS Cluster Setup Using Terraform

This project demonstrates how to set up an Amazon Elastic Kubernetes Service (EKS) cluster on AWS using Terraform.

## Prerequisites

Before you begin, ensure you have the following prerequisites installed:

- AWS CLI
- AWS IAM Role (not root, for security purposes)
- Terraform

## Steps

1. **Download AWS CLI**
   - Download and install the AWS Command Line Interface (CLI) from the official AWS website: https://aws.amazon.com/cli/

2. **Configure AWS CLI for the Role**
   - Configure the AWS CLI with your IAM role credentials for enhanced security.

3. **Download Terraform**
   - Download the latest version of Terraform from the official website: https://www.terraform.io/downloads.html

4. **Add Terraform to PATH**
   - Add the Terraform executable to your system's PATH environment variable for easy access.

5. **Write Terraform Configuration Files**
   - Create the following Terraform configuration files:
     - `main.tf`: Contains the main Terraform configuration for the EKS cluster.
     - `variables.tf`: Defines the required variables for the Terraform configuration.
     - `network.tf`: Configures the networking resources (VPC, subnets, etc.) for the EKS cluster.

6. **Initialize and Apply Terraform**
   - Run the following Terraform commands:
     - `terraform init`: Initializes the Terraform working directory.
     - `terraform plan`: Previews the changes that Terraform will make.
     - `terraform apply`: Applies the configured changes and creates the EKS cluster on AWS.

7. **Connect to the EKS Cluster**
   - After the EKS cluster is created, connect to it using the AWS CLI:
     ```
     aws eks --region us-west-2 update-kubeconfig --name my-eks-cluster
     ```

8. **AWS**
   ![AWS Management Console](Images/runnign-EKS.PNG)

9. **LENS SETUP**

   ***LENS recognize the cluster automatically***
   ![LENS](Images/LENS_1.PNG)

   ***Cluster running***
   ![LENS](Images/LENS.PNG)

   ***Pods Running***
   ![LENS](Images/jenkins-running.PNG)

## License

This project is licensed under the [MIT License](LICENSE).