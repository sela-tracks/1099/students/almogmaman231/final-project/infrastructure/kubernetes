#Locations
variable "region" {
  type = string
  default = "us-west-2"
}
variable "availability_zone_a" {
  type = string
  default = "us-west-2a"
}
variable "availability_zone_b" {
  type = string
  default = "us-west-2b"
}

#Compute
variable "instance_type" {
  type = string
  default = "t3.large"
}
variable "desired_size" {
    type = number
    default = 2
}
variable "min_size" {
    type = number
    default = 1
}
variable "max_size" {
    type = number
    default = 3
}

#Networking
variable "all_ip" {
  type = string
  default = "0.0.0.0/0"
}
variable "vpc_cidr" {
  type = string
  default = "10.0.0.0/16"
}
variable "eks_subnet_a_cidr" {
  type = string
  default = "10.0.1.0/24"
}
variable "eks_sunber_b_cidr" {
  type = string
  default = "10.0.2.0/24"
}

#Storage


#Modules
variable "csi_driver_module_source" {
  type = string
  default = "bootlabstech/fully-loaded-eks-cluster/aws//modules/kubernetes-addons/aws-ebs-csi-driver"
}
variable "csi_driver_module_version" {
  type = string
  default = "1.0.7"
}